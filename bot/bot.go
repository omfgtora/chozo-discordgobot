package bot

import (
	"fmt"
	"os"
	"strings"
	"time"

	"../config"
	"github.com/bwmarrin/discordgo"
	//"../features/bind"
	//"../features/ignoreChannel"
	//"../features/ignoreUser"
	//"../features/ping"
)

var (
	/*
		BotID is the session ID of the bot's discordgo session
		ServerID is the discord server ID
		Prefix is the character the bot looks for to determine if a command is being received
		Author is the ID of the user of the received message
		Timestamp is a timestamp in the format of YYYY-MM-DD HH:MM:SS
		BotSession is the session object of discordgo
	*/
	BotID       string
	ServerID    string
	Prefix      string
	Author      string
	Timestamp   string
	BotSession  *discordgo.Session
	channelList []string
)

// Connect creates a connection to the server using the token
func Connect() {

	now := time.Now()
	Timestamp := now.Format("2006-01-02 15:04:05")
	config.ReadSettings()
	err := config.ReadToken()

	bot, err := discordgo.New("Bot " + config.Token)

	if err != nil {
		fmt.Printf("Connection error: %v", err.Error())
		os.Exit(0)
		return
	}

	err = bot.Open()

	if err != nil {
		fmt.Printf("Connection error: %v", err.Error())
		os.Exit(0)
		return
	}

	user, err := bot.User("@me")

	if err != nil {
		fmt.Printf("Connection error: %v", err.Error())
		os.Exit(0)
		return
	}

	BotID = user.ID

	Prefix = config.BotPrefix

	fmt.Printf("Prefix: %v\n", Prefix)

	bot.AddHandler(messageHandler)
	//bot.AddHandler(Speak)

	fmt.Printf("[%v] Bot is running.\n--------------------------------------\n", Timestamp)

}

// Message handler. Checks for prefix, checks permissions (todo), looks through features for response (todo)
func messageHandler(session *discordgo.Session, message *discordgo.MessageCreate) {

	// BotSession := session

	// listChannels(BotSession)

	Author = message.Author.Username + "#" + message.Author.Discriminator

	// fmt.Printf("Guild: %v\n", session.Guild("Channels"))

	now := time.Now()
	Timestamp := now.Format("2006-01-02 15:04:05")

	// Print session
	//fmt.Printf("Session: %v\n", )

	// If message from bot log to console
	if message.Author.ID == BotID {
		fmt.Printf("[%v] <Chozo> %v\n", Timestamp, message.Content)
		return
	}

	// Responds to mention
	if len(message.Mentions) > 0 {
		for _, i := range message.Mentions {
			if i.ID == BotID {
				_, _ = session.ChannelMessageSend(message.ChannelID, "Hi")
			}
		}
	}

	// Logs chats to console
	fmt.Printf("[%v] {%v} %v: %v\n", Timestamp, message.ChannelID, message.Author, message.Content)
	if len(message.Attachments) > 0 {
		fmt.Printf("Attachments: %v\n", message.Attachments[0].URL)
	}

	// Will stop processing of prefix not found
	// if checkPrefix(message.Content) == false {
	// 	return
	// }

	time.Sleep(20 * time.Millisecond)

	// Ping Pong
	if message.Content == (Prefix + "ping") {
		_, _ = session.ChannelMessageSend(message.ChannelID, "Pong?")
	}

	// Quit
	if message.Content == (Prefix+"quit") && checkAdmin(Author) == true {
		_, _ = session.ChannelMessageSend(message.ChannelID, "Goodbye.")
		CloseSession(session, Timestamp)
		return
	} else if message.Content == (Prefix+"quit") && checkAdmin(Author) == false {
		_, _ = session.ChannelMessageSend(message.ChannelID, "No thanks.")
	}

}

// func listChannels(BotSession *discordgo.Session) {
// 	for v := range BotSession.Guild(Channels) {
// 		channelsList = append(channelsList, v)
// 	}
// }

func checkPrefix(Message string) bool {
	if strings.HasPrefix(Message, config.BotPrefix) == true {
		return true
	}
	return false
}

func checkAdmin(Author string) bool {
	if Author == config.Admin {
		return true
	}
	return false
}

// CloseSession will close the session, print a timestamp, and exit the program
func CloseSession(session *discordgo.Session, Timestamp string) {
	session.Close()
	fmt.Printf("\n------[%v] Disconnected------\n", Timestamp)
	os.Exit(0)
	return
}

// func Speak(session *discordgo.Session, message *discordgo.MessageCreate, message string) {
// 	_, _ = session.ChannelMessageSend("489951657883467787", message)
// }
