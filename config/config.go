package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

var (
	Token     string
	BotPrefix string
	Admin     string
	ServerID  string

	config *configStruct
)

type configStruct struct {
	Token     string `json:"Token"`
	BotPrefix string `json:"Prefix"`
	Admin     string `json:"Admin"`
	ServerID  string `json:"ServerID"`
}

func ReadToken() error {
	fmt.Println("Getting token...")

	tokenFile, err := ioutil.ReadFile("./config/token.json")

	if err != nil {
		fmt.Printf("Token read error: %v", err.Error())
		return err
	}

	err = json.Unmarshal(tokenFile, &config)

	if err != nil {
		fmt.Printf("Token json error: %v", err.Error())
		return err
	}

	Token = config.Token

	return nil
}

// ReadSettings gets settings from config/settings.json
func ReadSettings() error {
	fmt.Println("Getting settings...")

	settingsFile, err := ioutil.ReadFile("./config/settings.json")

	if err != nil {
		fmt.Printf("Settings read error: %v", err.Error())
		return err
	}

	err = json.Unmarshal(settingsFile, &config)

	if err != nil {
		fmt.Printf("Settings json error: %v", err.Error())
		return err
	}

	BotPrefix = config.BotPrefix
	Admin = config.Admin

	return nil
}
