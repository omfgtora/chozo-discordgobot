package main

import (
	"./bot"
	//"bufio"
	"fmt"
	//"os"
)

func main() {

	fmt.Printf(`
 ######  ##     ##  #######  ########  #######
##    ## ##     ## ##     ##      ##  ##     ##
##       ##     ## ##     ##     ##   ##     ##
##       ######### ##     ##    ##    ##     ##
##       ##     ## ##     ##   ##     ##     ##
##    ## ##     ## ##     ##  ##      ##     ##
 ######  ##     ##  #######  ########  #######
 by ToRA
` + "\n")

	bot.Connect()

	// reader := bufio.NewReader(os.Stdin)
	// input := reader.ReadString('\n')

	<-make(chan struct{})
	return

}
